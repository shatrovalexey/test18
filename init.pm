package main ;

use strict ;
use warnings ;
use DBI ;
use JSON ;
use IO::Handle ;
+ 1 ;

sub import( $;@ ) {
	my ( $class , $cmd , @args ) = @_ ;

	$::config = eval {
		local $/ = undef( ) ;

		open( my $fh , '<:encoding(utf-8)' , 'config.json' ) and
		JSON->new( )->utf8( 0 )->decode( <$fh> ) or die( $! ) ;
	} or die( 'Не удаётся загрузить конфигурацию: ' . $@ ) ;

	return +( ) unless $cmd eq 'run' ;

	do $_ foreach keys( %{ $::config->{ 'task' }{ 'run' } } ) ;
}

BEGIN {
	$::dbh = DBI->connect( @{ $::config->{ 'db' } }{ + qw(dsn login passwd options) } ) ;
	$::dbh->do( << "." ) ;
SET NAMES $::config->{ 'charset' } ;
.
} END {
	$::dbh->disconnect( ) ;
}