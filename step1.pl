#!/usr/bin/perl

use strict ;
use warnings ;
use init ;

eval { $::dbh->do( << '.' ) } ;
ALTER TABLE
	`shop_cats`
ADD
	`alt_count` TINYINT( 2 ) UNSIGNED DEFAULT 5 ;
.

$::dbh->do( $_ ) foreach << '.' , << '.' ;
DROP TABLE IF EXISTS `shop_cats2tags` ;
.
CREATE TABLE IF NOT EXISTS `shop_cats2tags`(
	PRIMARY KEY( `shop_cats_id` )
) AS
SELECT
	`sc1`.`id` AS `shop_cats_id` ,
	`st1`.`tag` ,
	concat_ws( ' ' , `sc1`.`name` , `st1`.`tag` ) AS `anchor` ,
	concat_ws( '/' , 'catalog' , `sc1`.`url` , `st1`.`en` ) AS `url`
FROM
	`shop_cats` AS `sc1`

	INNER JOIN `shop_products` AS `sp1` ON
	( `sp1`.`cat` = `sc1`.`id` )

	INNER JOIN `shop_tags2products` AS `stp1` ON
	( `sp1`.`id` = `stp1`.`prod_id` )

	INNER JOIN `shop_tags` AS `st1` ON
	( `st1`.`id` = `stp1`.`tag_id` )
GROUP BY
	1 ;
.