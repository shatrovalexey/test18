#!/usr/bin/perl

use strict ;
use warnings ;
use init ;

$::dbh->do( $_ ) foreach << '.' , << '.' ;
DROP TABLE IF EXISTS `shop_products_alt` ;
.
CREATE TABLE IF NOT EXISTS `shop_products_alt` (
	`shop_products_id` INT( 11 ) UNSIGNED NOT null ,
	`shop_products_id_alt` INT( 11 ) UNSIGNED NOT null ,
	`url` VARCHAR( 1000 ) NOT null ,
	PRIMARY KEY( `shop_products_id` , `shop_products_id_alt` )
)
ENGINE = InnoDB ,
DEFAULT CHARSET = cp1251 ;
.

my ( $sel_sth , $ins_sth ) = map $::dbh->prepare( $_ ) ,  << '.' , << '.' ;
SELECT
	`sp1`.`id` AS `shop_products_id` ,
	`sc1`.`alt_count` ,
	group_concat( `sp2`.`id` ORDER BY rand( ) ASC ) AS `shop_products_id_alt`
FROM
	`shop_products` AS `sp1`

	INNER JOIN `shop_tags2products` AS `stp1` ON
	( `sp1`.`id` = `stp1`.`prod_id` )

	INNER JOIN `shop_tags2products` AS `stp2` ON
	( `stp1`.`tag_id` = `stp2`.`tag_id` )

	INNER JOIN `shop_products` AS `sp2` ON
	( `sp1`.`id` = `stp2`.`prod_id` ) AND
	( `sp1`.`cat` = `sp2`.`cat` ) AND
	( `sp1`.`id` <> `sp2`.`id` )

	INNER JOIN `shop_cats` AS `sc1` ON
	( `sc1`.`id` = `sp1`.`cat` )
GROUP BY
	1 ;
.
INSERT IGNORE INTO
	`shop_products_alt`(
		`shop_products_id` ,
		`shop_products_id_alt` ,
		`url`
	)
SELECT
	? AS `shop_products_id` ,
	`sp1`.`id` AS `shop_products_id_alt` ,
	concat_ws( '/' , `sc1`.`url` , `sp1`.`url` ) AS `alt_url`
FROM
	`shop_products` AS `sp1`

	INNER JOIN `shop_cats` AS `sc1` ON
	( `sc1`.`id` = `sp1`.`cat` )
WHERE
	( `sp1`.`id` = ? ) ;
.

$sel_sth->execute( ) ;

while ( my ( $shop_products_id , $alt_count , $shop_products_id_alts ) = $sel_sth->fetchrow_array( ) ) {
	$ins_sth->execute( $shop_products_id , $& ) while
		( $alt_count -- > 0 ) && ( $shop_products_id_alts =~ m{\d+}gcs ) ;
}
$_->finish( ) foreach $ins_sth , $sel_sth ;